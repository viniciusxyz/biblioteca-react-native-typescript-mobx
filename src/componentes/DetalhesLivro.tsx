import * as React from 'react'
import { ILivros } from './Livro';
import { IStore } from './store/Store';
import { Text, Image } from 'react-native';
import { Card, Button } from 'react-native-elements';
import { inject, observer } from 'mobx-react';

@inject("StoreAPP")
@observer
class DetalhesLivro extends React.Component<IStore & ILivros> {
    public render() {
        return (
            <Card>
                <Text>{this.props.TituloLivro}</Text>
                <Image
                    source={this.props.Imagem}

                />
                <Text>{this.props.Conteudo}</Text>
                <Button
                    title={"R$ " + this.props.Valor.toFixed(2)}
                    onPress={this.props.StoreAPP.AddItemCarrinho(this.props)}
                />
            </Card>
        )
    }
}

export default DetalhesLivro as React.ComponentType<{}>